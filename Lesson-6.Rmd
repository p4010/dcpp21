---
title: "Lesson 6 --- 20211123"
output: 
  pdf_document: 
    toc: yes
    number_sections: yes
---

# Factorial plans

## Battery life experiment

We want to study the relationship between battery life, temperature, and type of electrolyte in batteries.

We build a $3^2$ factorial plan, repeating each treatment 4 times. Total 4 times 9 tests.

### Data preparation

Prepare the data frame:

```{r}
df <- expand.grid(
  Temperature = c(15, 75, 125),
  Material = LETTERS[1:3],
  Repeat = 1:4,
  Response = NA
)
N <- length(df$Temperature)
df <- data.frame(
  RunOrder = sample(N),
  StdOrder = 1:N,
  df
)
write.table(df[order(df$RunOrder),], "battery.txt", row.names = F, quote=F)
```

If we prefer CSV format (to be open with Excel), we can do:

```{r eval=FALSE, include=TRUE}
# Excel set to english: separator is comma, decimal is point
write.csv(df[order(df$RunOrder),], "battery_en.csv", quote=F, row.names=F)
# Excel set in italian: separator is semicolon, decimal is comma
write.csv2(df[order(df$RunOrder),], "battery_it.csv", quote=F, row.names=F)
```

Do the experiments and then read back the data:

```{r}
df <- read.table("http://repos.dii.unitn.it:8080/data/battery.dat", header=T)
head(df)
```
Correct the dataframe column `Material`, which we want to be A, B or C rather than 1, 2, or 3:

```{r}
df$Material <- LETTERS[df$Material]
head(df)
```
### Screening

Evaluate the interactions between factors with an interaction plot. Note that any conclusion is subjected to the **verification of statistical significance** via ANOVA.

```{r}
interaction.plot(df$Temperature, df$Material, df$Response,
                 xlab="Temperature (°C)", 
                 ylab="Batery life (min)",
                 trace.lab="Material")
```
### ANOVA

Let's do the ANOVA analysis in order to verify if there are significant treatments in our experiment. We want to model the yield as depending on the two factors and their interaction:

$$
y_{ijk} = \alpha_i + \beta_j + (\alpha\beta)_{ij} + \epsilon_{ijk}
$$
In R, the equivalent of such a model is the formula `Y~A+B+A:B` or, briefly, `Y~A*B`. In other words:

* `A+B` means take effects of factor A and B, separately ($\alpha_i + \beta_j$)
* `A:B` represents the interaction ($(\alpha\beta)_{ij}$)
* `A+B+A:B` is equal to `A*B`

Also, we need to remember that all factors must be categorical: if any of the factors is numeric, we need to cast it to a `factor` type with the `factor()` function:

```{r}
df$Temperature <- factor(df$Temperature)
df.lm <- lm(Response ~ Temperature*Material, data=df)
anova(df.lm)
```

Looking at the ANOVA table it seems that both factors and their interaction are statistically significant.

A set of Tukey's tests can help analyzing the interaction plot above obtained:

```{r}
for (t in levels(df$Temperature)) {
  cat("Temperature: ", t, "\n")
  print(TukeyHSD(aov(Response~Material, data=df[df$Temperature==t,])))
}
```

We can conclude that:
* at 15°C, the three electrolytes are equivalent
* at 70°, electrolytes b and c are equivalent, and a is significantly worst
* at 125°, only c is marginally better.

Of course it is also interesting to do the Tukey's tests on the orthogonal view:
```{r}
interaction.plot(df$Mat, df$Temp, df$Response,
                 xlab="Material",
                 ylab="Battery life (h)",
                 trace.lab="Temperature (°C)")
# Tukey's test
for (m in LETTERS[1:3]) {
  cat("Material: ", m, "\n")
  print(TukeyHSD(aov(Response~Temperature, data=df[df$Material == m,])))
}
```

### Model Adequacy Cecks

Let us verify the assumption of normality of residuals:

```{r}
library(car)
qqPlot(df.lm$residuals)
shapiro.test(df.lm$residuals)
```

Residuals appear to be normally distributed. Now let's see if there are **patterns**: we plot the residuals against run and standard order, against fitted values and against factor levels:

```{r}
plot(df$RunOrder, df.lm$residuals, xlab="Run Order", ylab="Residuals")
plot(df$StandardOrder, df.lm$residuals, xlab="Std Order", ylab="Residuals")
plot(df.lm$fitted.values, df.lm$residuals, xlab="Fitted values", ylab="Residuals")
plot(as.numeric(df$Temperature), df.lm$residuals, xlab="Temp", ylab="Residuals")
```

If we observe any pattern, the meaning is:

* in residuals vs. run order: the noise was increasing during the test sequence, typically due to a change in environmental conditions. This does not invalidate the experiment, but reduces the *power* of the F-test (i.e., a larger evidence is needed for a similar p-value). A more controlled testing environment/protocol is needed.
* in residuals vs. std order: the noise level depends on the level of one of the factors. This should happen when controlling the experiment at a given factor level becomes more difficult (e.g., controlling the temperature at 125°C is more difficult than at lowe temperatures)
* in residuals vs. fitted values: the statistical model is not adequate. This typically requires a **transformation** of the yield (see next)

In our present case, there is no pattern in the plot, so we can accept the last model.

### Regression

We can do regression on the only *quantitative* factor, `Temperature`:

```{r}
# Convert back Temperature to a numerical vector
df$Temperature <- as.numeric(as.character(df$Temperature))
# create a new grid of regressor points for which we want to evaluate the model:
df.new <- expand.grid(
  Temperature=seq(min(df$Temp), max(df$Temp), len=50),
  Material=levels(factor(df$Material))
)

# add to the new grid the result of predict() (three columns)
# Note that the R formula cannot be Response~Temperature^2*Material, because that 
# would expand to: R~T^2*M => R~T*T*M => R~T+T+T:T+M, which is not what we need
# and that is why we need the Identity operator I(): R~I(T^2)*M => R~I(T^2)+M+I(T^2)*M
df.new <- data.frame(
  df.new,
  predict(lm(Response~I(Temperature^2)*Material, data=df), newdata=df.new, interval="conf")
)

# We plot the original points, plus the quadratic models for the three materials
# and the confidence limits.
colors = c("red", "green", "blue")
plot(Response~Temperature, data=df[df$Mat=="A",], 
     col=colors[1], 
     ylim=range(df$Response)+c(-10,10), 
     main="Quadratic fit"
)
points(Response~Temperature, data=df[df$Mat=="B",], col=colors[2])
points(Response~Temperature, data=df[df$Mat=="C",], col=colors[3])

# Now plot fits and confidence bands:
for (i in 1:3) {
  lines(fit~Temperature, data=df.new[df.new$Mat==LETTERS[i],], col=colors[i])
  lines(lwr~Temperature, data=df.new[df.new$Mat==LETTERS[i],], col=colors[i], lty=2)
  lines(upr~Temperature, data=df.new[df.new$Mat==LETTERS[i],], col=colors[i], lty=2)
}
# Add a legend and a grid:
legend("topright", legend=LETTERS[1:3], lty=1, col=colors)
grid()
```

## Central Composite Design
Taken from Montgomery's book, 5th ed. page 273.

We have a chemical reactor that produces a compound by a suitable reaction. We want to study the process by relating the reaction yield with temperature and time. 

We design a full factorial plan $2^2$: factor A is the reaction time, factor B is the reaction temperature. Response is the reaction yield.

Create the design matrix and add the data for a un-replicated design:

```{r}
lvl <- c("-", "+")
df <- expand.grid(A=lvl, B=lvl)
df$Y <- c(39.3, 40.9, 40.0, 41.5) # order: (I) a b ab
df
```

We can look at what's happening with an interaction plot:

```{r}
interaction.plot(df$A, df$B, df$Y, xlab="Temp.")
```
Both factors seem to have a positive effect, and given that the two lines are parallel, there seems to be no interaction. Of course, we cannot accept these conclusion without the ANOVA.

As the factorial plan is non-replicated, though, we cannot perform the ANOVA. 

```{r}
df.lm <- lm(Y~A*B, data=df)
anova(df.lm)
```

As we see, it fails providing F-values.

We need to either replicate the experiment at least twice, or to add a central point in the origin. The latter approach has the advantage to allow checking for curvature in the response surface.

Se we add a central point repeated 5 times, and convert categorical factors to numerical ones in order to perform a quadratic fit:

```{r}
df$A <- ifelse(df$A=="-", -1, +1)
df$B <- ifelse(df$B=="-", -1, +1)
df[5:9,] <- 0
df$Y[5:9] <- c(40.3, 40.5, 40.7, 40.2, 40.6)
```

Firstly, we check the normal first order model:

```{r}
df.lmc <- lm(Y~factor(A)*factor(B), data=df)
anova(df.lmc)
```

Now that we have repetitions, the ANOVA provides p-values, and it confirms that the interaction is not significant.

We can check for curvature fitting any quadratic model, in `A`, `B`, or both, correspondingly:

```{r}
df.lmc <- lm(Y~A*B+I(A^2), data=df)
anova(df.lmc)
df.lmc <- lm(Y~A*B+I(B^2), data=df)
anova(df.lmc)
```

Both ANOVA tables confirm that there is no quadratic effect. Also note that the two tables contain identical values.


## Cutting process
Montgomery, 5th ed. page. 276

We want to investigate the cutting process in a lathe with a $3\times 2^3$ full factorial plan. The process factors are:

* A = cutting speed
* B = tool geometry
* C = cutting angle
* r = replicate (3 times)
* Yield = tool life

Design matrix:
```{r}
lvl <- c("-", "+")
df <- expand.grid(r=1:3, A=lvl, B=lvl, C=lvl, Y=NA)
df$Y <- c(
  22, 31, 25, 32, 43, 29,
  35, 34, 50, 55, 47, 46,
  44, 45, 38, 40, 37, 36,
  60, 50, 54, 39, 41, 47
)
head(df)
```

Analysis of variance:
```{r}
df.lm <- lm(Y~A*B*C, data=df)
anova(df.lm)
```

Revise the model by removing non-significant factors:
```{r}
df.lm2 <- lm(Y~A:C+B+C, data=df)
anova(df.lm2)
```

Check for model adequacy:
```{r}
library(car)
qqPlot(df.lm2$residuals)
shapiro.test(df.lm2$residuals)
plot(df.lm2$fit, df.lm2$residuals, xlab="fitted values", ylab="Residuals")
```
There are no evident patterns, so we accept the model as adequate.


## Drilling process
Montgomery, 5th ed. page 257.

We want to study a ground drilling process in an un-replicated full factorial plan, where the factors are:

* A = drilling load
* B = mud flow rate
* C = rotational speed
* D = type of drilling mud
* Y = drilling rate

```{r}
lvl <- c("-", "+")
df <- expand.grid(A=lvl, B=lvl, C=lvl, D=lvl, Y=NA)
head(df)
length(df$A)
```

Add the yield in standard order:

```{r}
df$Y <- c(
  1.68, 1.98, 4.98, 5.70,  # (1) a b ab
  3.24, 3.44, 9.97, 9.07,  # c ac bc abc
  2.07, 2.44, 7.77, 9.43,  # d ad bd abd
  4.09, 4.53, 11.75, 16.30 # cd acd bcd abcd
)
# df$Yfake <- rnorm(16) # to test for Daniel's method
sum(df$Y) # checksum
```

Let's build a complete linear model. The plan is unreplicated, so we cannot do the ANOVA and we have to rely on the Daniel's method:

```{r}
df.lm <- lm(Y~A*B*C*D, data=df)
# Daniel's method:
effects <- df.lm$effects[2:length(df.lm$effects)]
(qq <- qqnorm(effects))
qqline(effects)
text(qq$x, qq$y, labels=names(effects))

# The FrF2 library provides a dedicated function:
library(FrF2)
DanielPlot(df.lm, alpha=0.1)
```

By looking at the Daniel's plot, it appears that significant factors are B, B:C, A, B:D, D, C. So we can formulat a new, reduced model, which gains enough redundancy to perform the ANOVA:

```{r}
df.lm <- lm(Y~B*C+B*D, data=df)
anova(df.lm)
```

The model adequacy check shows that the residuals are normal, but there is an evident pattern:

```{r}
qqPlot(df.lm$residuals)
plot(df.lm$fitted.values, df.lm$residuals)
```

We can try to remove the pattern by applying a transformation to the yield: we try and rise the yield to different powers until we find a transformation that removes the pattern.

There is a formal method named after Box-Cox, which allows to identify the proper transformation graphically:

```{r}
library(MASS)
(bc <- boxcox(Y~B*C+C*D, data=df))
```
The maximum of the Box-Cox curve is at {r bc$x[which.max(bc$y)]}: this means that the optimum transformation would be `Y^-0.424242`. Rather than adopting that power, though, we prefer to select the closest *sensible* power that falls within the confidence interval: in our case `Y^-0.5`:

```{r}
df.lmt <- lm(Y^-0.5~B*C+D, data=df)
anova(df.lmt)
plot(df.lmt$fitted.values, df.lmt$residuals)
```
As we see, now the residuals are pattern free and so we can accept the last model.


## Fractional factorial plan

We want to study the yield of an IC manufacturing plant according to:

* A = aperture
* B = exposure time
* C = develop time
* D = mask dimension parameter
* E = etch time
* Y = response

We design a  $2^{5-1}_{IV}$ unreplicated factorial plan with the defining relationship $I=ABCDE$.

```{r}
lvl <- c(-1,1)
df <- expand.grid(A=lvl, B=lvl, C=lvl, D=lvl) # E=ABCD
attach(df)
df$E <- A*B*C*D # E=ABCD
detach(df)
for (f in LETTERS[1:5]) df[[f]] <- factor(df[[f]])
df$Y <- c(
  8, 9, 34, 52,
  16, 22, 45, 60,
  6, 10, 30, 50,
  15, 21, 44, 63
)
sum(df$Y)
```

The plan is unreplicated, so we need to use the Daniel's method:

```{r}
df.lm <- lm(Y~A*B*C*D*E, data=df)
effects <- df.lm$effects[2:length(df.lm$effects)]
(qn <- qqnorm(effects))
qqline(effects)
text(qn$x, qn$y, labels=names(effects))
```

The proper model is much probably `Y~A*B+C`. To be conservative, we can still use:

```{r}
df.lm <- lm(Y~A*B*C, data=df)
anova(df.lm)
```

Which confirms that the proper model is `Y~A*B+C`.

```{r}
df.lm <- lm(Y~A*B+C, data=df)
anova(df.lm)
```

Now we need to check model adequacy:

```{r}
library(car)
qqPlot(df.lm$residuals)
plot(df.lm$fitted.values, df.lm$residuals)
```

We can accept the model and look at the interactions:

```{r}
attach(df)
interaction.plot(A, B, Y, xlab="Aperture", ylab="Yield", trace.lab="Exposure time")
interaction.plot(A, C, Y, xlab="Aperture", ylab="Yield", trace.lab="Develop time")
interaction.plot(B, C, Y, xlab="Exposure time", ylab="Yield", trace.lab="Develop time")
detach(df)
```

# Injection molding
Parts manufactured in an injection molding process tend to have excessive shrinkage. This is causing problems in the downstream assembly process. We need to understand how to change process parameters in order to reduce shrinkage.

* mold temperature (A)
* screw speed (B)
* holding time (C)
* cycle time (D)
* gate size (E)
* holding pressure (F)

Each factor is tested at two levels, representing the technological limits of the process

We decide to do a $2^{6-2}_{IV}$ unreplicated factorial plan:

```{r}
lvl <- c(-1, 1)
df <- expand.grid(A=lvl, B=lvl, C=lvl, D=lvl)
attach(df) # defining relationships: I=ABCE, I=BCDF
  df$E <- A*B*C
  df$F <- B*C*D
detach(df)
for (f in LETTERS[1:6]) df[[f]] <- factor(df[[f]])
df$Y <- c(
  6, 10, 32, 60,
  4, 15, 26, 60,
  8, 12, 43, 60,
  16, 5, 37, 52
)
sum(df$Y)
```

Being an unreplicated design we apply the Daniel's method:

```{r}
df.lm <- lm(Y~A*B*C*D*E*F, data=df)
DanielPlot(df.lm, alpha=0.1)
```

We revise the model and do the ANOVA:

```{r}
df.lm <- lm(Y~A*B+A*D, data=df)
anova(df.lm)
```

The proper model is thus `Y~A*B+A:D`. Note that the defining relationships `I=ABCE`, `I=BCDF` point out aliases between `A` and `BCE`, `B` and `ACE`, `B` and `CDF`, `AB` and `CE`. Thanks to the sparsity of effects principle, we can pick `A`, `B`, and `AB` over all the higher order effects.

```{r}
df.lm <- lm(Y~A*B+A:D, data=df)
anova(df.lm)
qqPlot(df.lm$residuals)
plot(df.lm$fitted.values, df.lm$residuals)
```

```{r}
attach(df)
  interaction.plot(A, B, Y, xlab="Mold temp", ylab="Shrinkage", trace.lab="Screw speed")
  interaction.plot(A, D, Y, xlab="Mold temp", ylab="Shrinkage", trace.lab="Cycle time")
detach(df)
```

Looking at the interaction plot we can decide to set `A` and `B` both at low level in order to minimize the shrinkage.

It is also interesting to look at the plot of residuals versus factors:

```{r}
plot(df$A, df.lm$residuals)
plot(df$B, df.lm$residuals)
plot(df$C, df.lm$residuals)
plot(df$D, df.lm$residuals)
plot(df$E, df.lm$residuals)
plot(df$F, df.lm$residuals)
```

Among all these plots, only the factor `D` seems to have a pattern: even if the factor alone is not significant, it is evident that keeping `D` at low level reduces the spreading of residuals, which is as to say that it reduces the variability of the process (i.e., the shrinkage has a lower variability at `D` low). So we also select `D` at low level.



