---
title: "Lesson 5 --- 20211119"
output: 
  pdf_document: 
    toc: yes
    number_sections: yes
---

# Linear regression

We build a single-factor experiment for showing how to use the `lm()` function to build linear models.

Let's assume to have a dependent variable $y$ related to a predictor (or independent variable) $x$ quadratically, but with a small contribution of the quadratic term. We build a dataframe collecting the data and perturbing the nominal relationship $y=2x+0.01x^2$ with a normal noise.

```{r}
set.seed(1)
N <- 100
df <- data.frame(x=1:N)
df$yn <- 2*df$x + 0.01*df$x^2
df$y <- df$yn + rnorm(N, 0, 10)
# Now randomize the data collection order!
df$run <- sample(N)
df <- df[order(df$run),]
# plot(yn~x, data=df, typ="l", col="blue")
plot(y~x, data=df, typ="p")
```
Now, suppose that we don't know the nominal relationsip betwen $y$ and $x$.
Observing the data, we try to model the relationship $y=f(x)$ as a linear model:

```{r}
df.lm <- lm(y~x, data=df)
(df.lm.s <- summary(df.lm))
plot(y~x, data=df, typ="p")
abline(df.lm, col="red")
text(80, 70, lab=paste("R^2=", round(df.lm.s$r.squared, 3)))
```
Note that the function `abline()` can take as an argument a linear model: if it only has two coefficients, it uses those for plotting the line.

We observe that the p-value of the intercept is large, so we can try to improve the model by removing the constant term in the model (add `-1` to the formula `y~x`):

```{r}
df.lm <- lm(y~x-1, data=df)
(df.lm.s <- summary(df.lm))
plot(y~x, data=df, typ="p")
abline(df.lm, col="red")
text(80, 70, lab=paste("R^2=", round(df.lm.s$r.squared, 3)))
```
Note that the regression coefficient $R^2$ has increased, so removing the intercept improves the fitting.

Can we accept this model? not before doing a MAC!

```{r}
library(MASS) # for stdres()
shapiro.test(df.lm$residuals) # OK
plot(stdres(df.lm))
plot(df.lm$fitted.values, stdres(df.lm))
```
So the residuals are normal according to the Shapiro-Wilk test, but we observe a pattern in the residuals vs. fitted values plot.

When we observe a pattern in the residuals vs. fitted values plot, it often means that the model is non-adequate, i.e. its order is too small (under-fitting). So we move to a quadratic model:

```{r}
# in R formulas, A*B = A + B + A:B
df.lm <- lm(y~x+I(x^2)-1, data=df) # x^2 = x*x = x + x + x:x = x
(df.lm.s <- summary(df.lm))
plot(y~x, data=df, typ="p")
text(80, 70, lab=paste("R^2=", round(df.lm.s$r.squared, 3)))
curve(x*df.lm$coefficients[1] + x^2*df.lm$coefficients[2], col="red", add=T)
```

Now we verify the residuals, thet do not present any pattern, so **we can accept the quadratic model**:

```{r}
plot(df.lm$fitted.values, df.lm$residuals)
```
The linear model provided by `lm()` also contains information about the reliability of the model itself. This information can be graphically represented as confidence and prediction intervals, obtained from the `predict()` function. The latter wants a new dataframe containing the new values of the predictor $x$, for which we want to predict the dependent variable $y$. By default, it returns a **vector** of $y$. We can also ask for confidence or prediction intervals: in these cases, the output is a dataframe with three columns: fit, lower bound and upper bound.

```{r}
plot(y~x, data=df, typ="p", col="gray")
new <- data.frame(x=seq(1,100,5)) # new predictor values
new <- data.frame(new,            # add to the same dataframe fit, conf and pred
      conf=predict(df.lm, newdata=new, interval="conf", level=0.99),
      pred=predict(df.lm, newdata=new, interval="pred", level=0.95))
lines(new$x, new$conf.lwr, col="red")
lines(new$x, new$conf.upr, col="red")
lines(new$x, new$pred.lwr, col="blue")
lines(new$x, new$pred.upr, col="blue")
lines(conf.fit~x, data=new, col="green")
```
The **prediction interval** (blue band) holds 95% of observations: if you add a new observation, it has 95% probability of lying within the blue band.

The **confidence interval** (red band) is the range, within which the *nominal* model has 99% probability to fall.

Of the two, the confidence interval is typically the most important one.

# Logistic regression

We want to predict the survival of a bottle of liquid soap in a drop test, depending on its filling level. In fact, the larger the filling level the lower the amount of air in the bottle, which acts as a compressible fluid absorbing shocks.

We have a data frame for 400 drop tests at different filling level, as percent of nominal capacity. We begin visualizing the data.

```{r}
data <- read.table("http://repos.dii.unitn.it:8080/data/soap_bottles.txt", h=T)
head(data)
hist(data$p, xlab="Relative filling level (%)")
rug(data[!data$OK,]$p, col="red")
rug(data[data$OK,]$p, col="green")
```
The histogram presents two populations, and the rug plot on the bottom visualizes the fact that there is an overlapping between the two populations, i.e. some bottle are surviving with filling levels equal or larger than bottles that are not surviving the drop test.

## Regression

This type of regression is the simplest type of classification machine learning. In ML we typically want to split a dataset in two parts, one used for training the model, the other (usually smaller) for verifying its performance.

The library `caTools` provides a useful function for easily splitting a dataframe randomly, keeping the same relative quantity of labels in both subsets:

```{r}
library(caTools)
set.seed(1) # for sample.split uses random numbers
split <- sample.split(data$OK, SplitRatio=0.8)
train <- data[split,]
test <- data[!split,]
```

Now we build a logit model, by using the `glm` generalized linear model function, and specifying that we want to use the `binomial` family (the one that corresponds to a logistic function):

```{r}
model <- glm(OK~p, data=data, family="binomial")
summary(model)
```

This model fits the data with the *logistic function*:
$$
f(x)=\frac{1}{1+e^{-k(x-x_0)}}
$$
which is a sigmoid (or S-shaped) function and where $x_0$ is the midpoint and $k$ is the steepness. Note that the `glm` function actually uses a slightly different formulation, where $f(x)=1/(1+\exp(-kx-m))$, where $m=-kx_0$, so we can plot the fitted logistic curve using the model coefficients:

```{r}
k <- model$coefficients[2]
x0 <- -model$coefficients[1]/model$coefficients[2]
curve(1/(1+exp(-k*(x-x0))), xlim=c(99,100), ylab="f(x)", main="Logistic function")
abline(v=0, col="gray")
abline(h=0.5, col="gray")
```


## Prediction and validation

The model can be used for *predicting* the survival of a bottle depending on its filling level. We can test its reliability on the `test` fraction of the original data.

First, we plot the logistic regression function according to the observed survivals. This time, rather than plotting the fitted logistic function, we use `predict()` to calculate the fitted function values in a predetermined vector of predictors in the range 99-100:

```{r}
lvl <- seq(99,100, 0.01)
plot(lvl, predict(model, newdata=data.frame(p=lvl), type="response"),
     typ="l",
     xlab="Filling level (%)",
     ylab="Predicted survival probability")
points(OK~p, data=test[test$OK,], col="green")
points(OK~p, data=test[!test$OK,], col="red")
# We also add vertical rugs for showing predicted logistic values for survived 
# and failed bottles
test$fit <- predict(model, newdata=test, type="response")
rug(test[test$OK,]$fit, col="green", side=2)
rug(test[!test$OK,]$fit, col="red", side=4)
abline(h=0.5)
```
Secondly, we want to define the reliability of the model: to do so, we build a *confusion table* reporting the number of correct and wrong predictions, deciding that we predict a failure for all filling levels corresponding to a logit function smaller than 0.5:

```{r}
# in units:
table(Actual=test$OK, Predicted=test$fit>0.5)
# in percent:
(ct <- round(table(Actual=test$OK, Predicted=test$fit>0.5)/length(test$OK)*100, 1))
```

In particular, we observe that if we set a threshold of 0.5 in the classification, the model has a *false positive rate* (FPR) of `r ct[1,2]`%, and a *false negative rate* (FNR) of `r ct[2,1]`%. Typically, in classifiers where the survival rate is of interest, we want the false negative rate to be as small as possible, at the expense of increasing the false positive rate.

Of course, by changing the threshold we can change the balance between FNR and FPR The functions `prediction` and `performance` in the `ROCR` library allow to find a proper balance graphically:

```{r}
library(ROCR)
pred <- prediction(test$fit, test$OK)
perfn <- performance(pred, "tnr", "fnr")
plot(perfn, colorize=T, print.cutoffs.at=seq(0,1,0.1))
perfp <- performance(pred, "tpr", "fpr")
plot(perfp, colorize=T, print.cutoffs.at=seq(0,1,0.1))
```

From the first plot,we see that the FNR decreases significantly setting the threshold to 0.4 and that there is little gain from 0.4 to 0.2. On the second plot, though, we see that the FPR increases quickly in the rance 0.4--0.1. Consequently, we set the threshold to 0.4:

```{r}
(ct <- round(table(Actual=test$OK, Predicted=test$fit>0.4)/length(test$OK)*100, 1))
```


